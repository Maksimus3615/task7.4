package com.shpp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task74Application {

    public static void main(String[] args) {
        SpringApplication.run(Task74Application.class, args);
    }

}
